package com.github.immortaleeb.hearts.domain

import com.github.immortaleeb.hearts.vocabulary.Card
import com.github.immortaleeb.hearts.application.command.CouldNotPassCards
import com.github.immortaleeb.hearts.application.command.CouldNotPlayCard
import com.github.immortaleeb.hearts.vocabulary.Deck
import com.github.immortaleeb.hearts.application.command.GameHasNotStarted
import com.github.immortaleeb.hearts.application.command.GameHasStarted
import com.github.immortaleeb.hearts.application.command.PassCardsResponse
import com.github.immortaleeb.hearts.application.command.PassedCards
import com.github.immortaleeb.hearts.application.command.PlayCardResponse
import com.github.immortaleeb.hearts.application.command.PlayedCard
import com.github.immortaleeb.hearts.application.command.PlayerCouldNotJoin
import com.github.immortaleeb.hearts.application.command.PlayerJoinResponse
import com.github.immortaleeb.hearts.application.command.PlayerJoined
import com.github.immortaleeb.hearts.vocabulary.PlayerName
import com.github.immortaleeb.hearts.application.command.StartGameResponse
import com.github.immortaleeb.hearts.domain.GamePhase.ENDED
import com.github.immortaleeb.hearts.domain.GamePhase.PASSING_CARDS
import com.github.immortaleeb.hearts.domain.GamePhase.PLAYING
import com.github.immortaleeb.hearts.domain.GamePhase.WAITING_FOR_PLAYERS
import com.github.immortaleeb.hearts.domain.PassDirection.HOLD

internal const val NUMBER_OF_PLAYERS = 4

class Game(
    private val dealer: Dealer = ChunkedDealer,
    private val passingRule: PassingRule = FourWayPassing,
) {
    private val players = Players()
    private var currentRound: Round? = null
    private var currentDeal: Int = 0
    private var gamePhase = WAITING_FOR_PLAYERS
    private val cardsPassedDuringThisDeal: MutableMap<PlayerName, Set<Card>> = mutableMapOf()
    private val scoreboard: Scoreboard = Scoreboard()

    val currentPlayerName: PlayerName get() = currentRound!!.currentPlayerName
    val hasStarted get() = gamePhase == PLAYING
    val hasEnded get() = gamePhase == ENDED

    fun join(player: PlayerName): PlayerJoinResponse {
        if (players.size == NUMBER_OF_PLAYERS) return PlayerCouldNotJoin(player, "Game already has four players")

        players.add(player)
        scoreboard.addPlayer(player)

        return PlayerJoined
    }

    fun start(): StartGameResponse {
        if (players.size != NUMBER_OF_PLAYERS) return GameHasNotStarted("Not enough players joined")

        startNextDeal()

        return GameHasStarted
    }

    private fun startNextDeal() {
        dealCards(Deck().shuffled())
        if (shouldPassThisRound()) {
            startPassing()
        } else {
            startPlaying()
        }
    }

    private fun shouldPassThisRound() = currentPassDirection != HOLD

    private fun startPassing() {
        gamePhase = PASSING_CARDS
    }

    private fun startPlaying() {
        currentRound = Round.startWith(players)
        gamePhase = PLAYING
    }

    private val currentPassDirection get() = passingRule.passDirectionOnDeal(currentDeal)

    private fun dealCards(deck: Deck) {
        currentDeal++
        players.names.forEachIndexed { i, player ->
            val dealtCards = dealer.dealCardsToPlayer(deck, player, i)
            player(player).receiveCards(dealtCards)
        }
    }

    fun passCards(cardsToPass: Set<Card>, passedBy: PlayerName): PassCardsResponse {
        if (gamePhase != PASSING_CARDS) return CouldNotPassCards("Now is not the time to be passing cards")

        val from = player(passedBy)
        if (from.name in cardsPassedDuringThisDeal) return CouldNotPassCards("${from.name} already passed cards during this deal")
        if (cardsToPass.size != 3) return CouldNotPassCards("${from.name} needs to pass exactly three cards")
        if (!from.hasCards(cardsToPass)) {
            val missingCards = (cardsToPass - from.hand).joinToString(", ")
            return CouldNotPassCards("${from.name} does not have $missingCards")
        }

        from.takeCards(cardsToPass)
        cardsPassedDuringThisDeal[from.name] = cardsToPass

        if (allPlayersPassedCards()) {
            givePassedCardsToPlayers()
            startPlaying()
        }

        return PassedCards
    }

    private fun allPlayersPassedCards() = cardsPassedDuringThisDeal.size == NUMBER_OF_PLAYERS

    private fun givePassedCardsToPlayers() {
        cardsPassedDuringThisDeal.entries.forEach { (fromPlayer, passedCards) ->
            val toPlayer = player(players.playerNextTo(fromPlayer, currentPassDirection))
            toPlayer.receiveCards(passedCards)
        }
        cardsPassedDuringThisDeal.clear()
    }

    fun handOf(player: PlayerName) = players.handOf(player)

    fun playCard(card: Card, playedBy: PlayerName): PlayCardResponse {
        val currentRound = currentRound
        if (currentRound == null || currentRound.isFinished) return CouldNotPlayCard("It's not time to play cards yet")

        val result = currentRound.playCard(card, playedBy)
        if (result != PlayedCard) {
            return result
        }

        if (currentRound.isFinished) {
            scoreboard.updateScoresWith(currentRound.tricks)

            if (scoreboard.playerHasReachedMaxScore()) {
                endGame()
            } else {
                startNextDeal()
            }
        }

        return PlayedCard
    }

    private fun endGame() {
        gamePhase = ENDED
    }

    private fun player(name: PlayerName) = players[name]

    fun scoreOf(player: PlayerName): Int = scoreboard.scoreOf(player)

}
