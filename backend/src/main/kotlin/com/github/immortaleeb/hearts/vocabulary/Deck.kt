package com.github.immortaleeb.hearts.vocabulary

data class Deck(val cards: List<Card> = allCards()) {
    val size = cards.size
    fun shuffled(): Deck = Deck(cards.shuffled())
}

private fun allCards() = allCombinationsOf(Suit.entries, Symbol.entries, ::Card)

fun <S, T, R> allCombinationsOf(list1: Iterable<S>, list2: Iterable<T>, combine: (S, T) -> R): List<R> {
    val result = mutableListOf<R>()
    list1.forEach { x ->
        list2.forEach { y ->
            result.add(combine(x, y))
        }
    }
    return result
}