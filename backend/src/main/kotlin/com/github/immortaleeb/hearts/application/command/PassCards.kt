package com.github.immortaleeb.hearts.application.command

import com.github.immortaleeb.hearts.vocabulary.Card
import com.github.immortaleeb.hearts.vocabulary.PlayerName

class PassCards(val cards: Set<Card>, val passedBy: PlayerName) : Command<PassCardsResponse>

interface PassCardsResponse
object PassedCards : PassCardsResponse
data class CouldNotPassCards(val reason: String) : PassCardsResponse
