package com.github.immortaleeb.hearts.application.query

object HasGameStarted : Query<Boolean>
