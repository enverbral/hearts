package com.github.immortaleeb.hearts.application.query

import com.github.immortaleeb.hearts.domain.Game

class QueryExecutor(private val game: Game) {
    fun <Result> execute(query: Query<Result>) = when (query) {
        is HasGameStarted -> game.hasStarted as Result
        is CardsInHandOf -> game.handOf(query.player) as Result
        is WhoseTurnIsIt -> game.currentPlayerName as Result
        is WhatIsScoreOfPlayer -> game.scoreOf(query.player) as Result
        is HasGameEnded -> game.hasEnded as Result
        else -> error("Unknown query of type ${query::class.qualifiedName}")
    }
}