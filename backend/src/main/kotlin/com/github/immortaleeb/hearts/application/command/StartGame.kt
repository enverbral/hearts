package com.github.immortaleeb.hearts.application.command

class StartGame : Command<StartGameResponse>

sealed interface StartGameResponse
data object GameHasStarted : StartGameResponse
data class GameHasNotStarted(val reason: String) : StartGameResponse
