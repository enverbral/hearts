package com.github.immortaleeb.hearts.domain

fun interface PassingRule {
    fun passDirectionOnDeal(deal: Int): PassDirection
}

enum class PassDirection {
    LEFT, RIGHT, ACROSS, HOLD
}

object NoPassing : PassingRule {
    override fun passDirectionOnDeal(deal: Int) = PassDirection.HOLD
}

object AlwaysPassLeft : PassingRule {
    override fun passDirectionOnDeal(deal: Int) = PassDirection.LEFT
}

object FourWayPassing : PassingRule {
    override fun passDirectionOnDeal(deal: Int) = when ((deal - 1) % 4) {
        0 -> PassDirection.LEFT
        1 -> PassDirection.RIGHT
        2 -> PassDirection.ACROSS
        else -> PassDirection.HOLD
    }
}