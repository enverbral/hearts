package com.github.immortaleeb.hearts.domain

import com.github.immortaleeb.hearts.vocabulary.Card
import com.github.immortaleeb.hearts.vocabulary.PlayerName
import com.github.immortaleeb.hearts.vocabulary.Suit

internal class Player(val name: PlayerName, private var _hand: MutableList<Card> = mutableListOf()) {
    val hand: List<Card> get() = _hand
    fun receiveCards(cards: Collection<Card>) {
        _hand += cards
    }

    fun takeCards(cards: Collection<Card>) {
        if (!hasCards(cards)) error("$name does not have all of ${cards.joinToString(", ")} in their hand")
        _hand.removeAll(cards)
    }

    fun takeCard(card: Card) {
        if (!hasCard(card)) error("$name does not have $card in their hand")
        _hand.remove(card)
    }

    fun canFollowSuit(suit: Suit) = _hand.any { it.suit == suit }
    fun hasCards(cards: Collection<Card>) = _hand.containsAll(cards)
    fun hasCard(card: Card): Boolean = _hand.contains(card)
    fun hasSuitOtherThan(suit: Suit): Boolean = _hand.any { it.suit != suit }
}