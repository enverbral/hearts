package com.github.immortaleeb.hearts.domain

import com.github.immortaleeb.hearts.vocabulary.Card
import com.github.immortaleeb.hearts.vocabulary.Deck
import com.github.immortaleeb.hearts.vocabulary.PlayerName

fun interface Dealer {
    fun dealCardsToPlayer(deck: Deck, player: PlayerName, i: Int): List<Card>
}

object ChunkedDealer : Dealer {
    override fun dealCardsToPlayer(deck: Deck, player: PlayerName, i: Int): List<Card> {
        val dealtCards = deck.cards.chunked(deck.size / NUMBER_OF_PLAYERS)
        return dealtCards[i]
    }
}
