package com.github.immortaleeb.hearts

import com.github.immortaleeb.hearts.application.command.CommandExecutor
import com.github.immortaleeb.hearts.application.query.QueryExecutor
import com.github.immortaleeb.hearts.domain.Game

class Context(game: Game = Game()) {
    val commandExecutor = CommandExecutor(game)
    val queryExecutor = QueryExecutor(game)
}
