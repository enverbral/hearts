package com.github.immortaleeb.hearts.domain

import com.github.immortaleeb.hearts.vocabulary.Card
import com.github.immortaleeb.hearts.vocabulary.PlayerName

internal class Players {
    private val playersByName: MutableMap<PlayerName, Player> = mutableMapOf()
    private val playerOrder: MutableList<PlayerName> = mutableListOf()

    val size: Int get() = playersByName.size
    val names: Set<PlayerName> get() = playersByName.keys

    fun add(player: PlayerName) {
        playersByName[player] = Player(player)
        playerOrder.add(player)
    }

    fun findPlayerWithCardInHand(card: Card): PlayerName? = playersByName.entries.find { it.value.hasCard(card) }?.key

    fun handOf(player: PlayerName) = playersByName.getValue(player).hand

    fun playerToTheLeftOf(player: PlayerName): PlayerName = playerNextTo(player, PassDirection.LEFT)

    operator fun get(name: PlayerName) = playersByName.getValue(name)

    fun playerNextTo(player: PlayerName, direction: PassDirection): PlayerName {
        val offset = when (direction) {
            PassDirection.LEFT -> 1
            PassDirection.RIGHT -> -1
            PassDirection.ACROSS -> 2
            PassDirection.HOLD -> 0
        }

        return playerOrder[(playerOrder.indexOf(player) + offset + playerOrder.size) % playerOrder.size]
    }
}