package com.github.immortaleeb.hearts.domain

import com.github.immortaleeb.hearts.vocabulary.PlayerName

private const val MAX_SCORE = 100

internal class Scoreboard {
    private val scores: MutableMap<PlayerName, Int> = mutableMapOf()

    fun addPlayer(player: PlayerName) {
        scores[player] = 0
    }

    fun playerHasReachedMaxScore() = scores.values.any { it >= MAX_SCORE }

    fun scoreOf(player: PlayerName): Int = scores.getValue(player)

    fun updateScoresWith(tricks: PlayedTricks) {
        for (player in scores.keys) {
            val scoreInCurrentDeal = tricks.wonBy(player).sumOf { it.score }
            addScore(player, scoreInCurrentDeal)
        }
    }

    private fun addScore(player: PlayerName, score: Int) {
        scores[player] = scores.getValue(player) + score
    }
}