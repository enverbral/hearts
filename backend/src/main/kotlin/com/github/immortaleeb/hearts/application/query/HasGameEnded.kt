package com.github.immortaleeb.hearts.application.query

object HasGameEnded : Query<Boolean>
