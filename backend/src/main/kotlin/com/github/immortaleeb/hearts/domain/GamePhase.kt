package com.github.immortaleeb.hearts.domain

internal enum class GamePhase {
    WAITING_FOR_PLAYERS, PASSING_CARDS, PLAYING, ENDED
}