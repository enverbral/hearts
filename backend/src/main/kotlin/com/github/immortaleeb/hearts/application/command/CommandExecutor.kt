package com.github.immortaleeb.hearts.application.command

import com.github.immortaleeb.hearts.domain.Game

class CommandExecutor(private val game: Game) {
    @Suppress("UNCHECKED_CAST")
    fun <Result> execute(command: Command<Result>): Result = when (command) {
        is MakePlayerJoinGame -> game.join(command.player) as Result
        is StartGame -> game.start() as Result
        is PlayCard -> game.playCard(command.card, command.playedBy) as Result
        is PassCards -> game.passCards(command.cards, command.passedBy) as Result
        else -> error("Unknown command of type ${command::class.qualifiedName}")
    }

}