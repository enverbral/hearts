package com.github.immortaleeb.hearts.application.query

import com.github.immortaleeb.hearts.vocabulary.PlayerName

object WhoseTurnIsIt : Query<PlayerName>
