package com.github.immortaleeb.hearts.domain

import com.github.immortaleeb.hearts.vocabulary.Card
import com.github.immortaleeb.hearts.application.command.CouldNotPlayCard
import com.github.immortaleeb.hearts.application.command.PlayCardResponse
import com.github.immortaleeb.hearts.application.command.PlayedCard
import com.github.immortaleeb.hearts.vocabulary.PlayerName
import com.github.immortaleeb.hearts.vocabulary.Suit
import com.github.immortaleeb.hearts.vocabulary.Symbol
import com.github.immortaleeb.hearts.vocabulary.of

private const val TRICKS_PER_DEAL = 13

internal class Round private constructor(private val players: Players) {
    private var _currentPlayerName: PlayerName = determineFirstPlayer()
    private var _heartsHaveBeenPlayed = false
    private var currentTrick: Trick? = null
    private val currentPlayer get() = players[_currentPlayerName]

    val tricks = PlayedTricks()
    val currentPlayerName get() = _currentPlayerName
    val isFinished: Boolean get() = tricks.played == TRICKS_PER_DEAL

    private val heartsHaveNotBeenPlayedYet get() = !_heartsHaveBeenPlayed
    private val isFirstTrick: Boolean get() = tricks.played == 0
    private val isFirstCardOfTrick: Boolean get() = currentTrick == null

    private fun determineFirstPlayer(): PlayerName = players.findPlayerWithCardInHand(Symbol.TWO of Suit.CLUBS)!!
    private fun determineNextPlayer(trick: Trick) = if (trick.isFinished) trick.winner else players.playerToTheLeftOf(_currentPlayerName)

    fun playCard(card: Card, playedBy: PlayerName): PlayCardResponse {
        val ledSuit = currentTrick?.ledSuit
        val isFirstTurn = isFirstTrick && isFirstCardOfTrick

        if (playedBy != currentPlayerName) return CouldNotPlayCard("It's not $playedBy's turn to play")

        if (!currentPlayer.hasCard(card)) return CouldNotPlayCard("$currentPlayerName does not have $card in their hand")
        if (isFirstTurn && card != Symbol.TWO of Suit.CLUBS) return CouldNotPlayCard("$currentPlayerName must play 2♣️ on the first turn")
        if (ledSuit != null && card.suit != ledSuit && currentPlayer.canFollowSuit(ledSuit)) return CouldNotPlayCard("$currentPlayerName must follow suit")
        if (isFirstTrick && card.suit == Suit.HEARTS && currentPlayer.hasSuitOtherThan(Suit.HEARTS)) return CouldNotPlayCard("$currentPlayerName cannot play ♥️ on the first trick")
        if (card.suit == Suit.HEARTS && heartsHaveNotBeenPlayedYet && currentPlayer.hasSuitOtherThan(Suit.HEARTS)) return CouldNotPlayCard("$currentPlayerName cannot open with ♥️ until first ♥️ has been played")

        cardPlayed(card, playedBy)

        return PlayedCard
    }

    private fun cardPlayed(card: Card, playedBy: PlayerName) {
        currentPlayer.takeCard(card)

        val trick = if (isFirstCardOfTrick) Trick.leadWith(card, playedBy) else currentTrick!!.playCard(card, playedBy)
        _currentPlayerName = determineNextPlayer(trick)
        _heartsHaveBeenPlayed = _heartsHaveBeenPlayed || card.suit == Suit.HEARTS

        if (trick.isFinished) {
            tricks.add(trick)
            currentTrick = null
        } else {
            currentTrick = trick
        }
    }

    companion object {
        fun startWith(players: Players) = Round(players)
    }
}
