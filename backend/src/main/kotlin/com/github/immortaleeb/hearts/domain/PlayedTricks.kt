package com.github.immortaleeb.hearts.domain

import com.github.immortaleeb.hearts.vocabulary.PlayerName

internal class PlayedTricks {
    private val tricks: MutableList<Trick> = mutableListOf()
    val played get() = tricks.size

    fun add(trick: Trick) {
        tricks.add(trick)
    }

    fun wonBy(player: PlayerName) = tricks.filter { it.winner == player }
}