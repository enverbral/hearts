package com.github.immortaleeb.hearts.application.command

import com.github.immortaleeb.hearts.vocabulary.PlayerName

data class MakePlayerJoinGame(val player: PlayerName) : Command<PlayerJoinResponse>

sealed interface PlayerJoinResponse
data class PlayerCouldNotJoin(val player: PlayerName, val reason: String) : PlayerJoinResponse
data object PlayerJoined : PlayerJoinResponse
