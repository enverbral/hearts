package com.github.immortaleeb.hearts.application.command

import com.github.immortaleeb.hearts.vocabulary.Card
import com.github.immortaleeb.hearts.vocabulary.PlayerName

data class PlayCard(val card: Card, val playedBy: PlayerName): Command<PlayCardResponse>

sealed interface PlayCardResponse
data object PlayedCard : PlayCardResponse
data class CouldNotPlayCard(val reason: String): PlayCardResponse
