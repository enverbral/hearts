package com.github.immortaleeb.hearts.domain

import com.github.immortaleeb.hearts.vocabulary.Card
import com.github.immortaleeb.hearts.vocabulary.PlayerName

internal class Trick(private val playedCards: List<PlayedCard>) {
    val score: Int get() = playedCards.sumOf { it.penalty }
    val isFinished: Boolean get() = playedCards.size == NUMBER_OF_PLAYERS
    val winner: PlayerName get() = playedCards.filter { it.suit == ledSuit }.maxBy { it.rank }.playedBy
    val ledSuit get() = playedCards.first().suit

    fun playCard(card: Card, playedBy: PlayerName) = Trick(playedCards + PlayedCard(card, playedBy))

    data class PlayedCard(val card: Card, val playedBy: PlayerName) {
        val suit = card.suit
        val rank = card.rank
        val penalty = card.penalty
    }

    companion object {
        fun leadWith(card: Card, playedBy: PlayerName) = Trick(listOf(PlayedCard(card, playedBy)))
    }
}