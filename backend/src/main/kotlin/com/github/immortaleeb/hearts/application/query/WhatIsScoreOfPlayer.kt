package com.github.immortaleeb.hearts.application.query

import com.github.immortaleeb.hearts.vocabulary.PlayerName

data class WhatIsScoreOfPlayer(val player: PlayerName) : Query<Int>
