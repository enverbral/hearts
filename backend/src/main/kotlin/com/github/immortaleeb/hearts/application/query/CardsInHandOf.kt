package com.github.immortaleeb.hearts.application.query

import com.github.immortaleeb.hearts.vocabulary.Card
import com.github.immortaleeb.hearts.vocabulary.PlayerName

class CardsInHandOf(val player: PlayerName) : Query<List<Card>>


