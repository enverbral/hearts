# Hearts

## Checking for updates

Run `./gradlew dependencyUpdates` to get a list of all dependencies that can be updated

## Gradle
### Update gradle

1. Lookup the latest gradle version at https://docs.gradle.org/current/release-notes.html
2. Run `gradle wrapper --gradle-version $VERSION`

### Update dependencies

Open `gradle/libs.versions.toml` and update the version of the dependencies you want to update

## Java
### Update java

1. Lookup the latest release of the JDK at https://www.oracle.com/java/technologies/javase/jdk-relnotes-index.html
2. Replace the java version in the toolchain configuration:
```
kotlin {
  jvmToolchain(JDK_VERSION)
}
```

## Kotlin
### Update kotlin

1. Lookup the latest kotlin version compatible with the gradle version at https://kotlinlang.org/docs/gradle-configure-project.html#apply-the-plugin
2. Replace the version of `versions.kotlin` in `gradle/libs.versions.toml`

## Log4j2
### Update log4j2

1. Lookup the latest version at https://logging.apache.org/log4j/2.x/index.html
2. Change `versions.log4j` in `gradle/libs.versions.toml`
